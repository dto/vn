
(defvar vn-directory "~/vn")

(defvar vn-colors ())

(setf vn-colors
'("red" "OrangeRed" "DarkOrange" "orange" "gold" "yellow" "chartreuse" "LawnGreen"
  "green" "SpringGreen" "MediumSpringGreen" "cyan" "DeepSkyBlue" "blue" "MediumBlue"
  "DarkViolet" "DarkMagenta" "magenta" "LightCoral" "salmon" "aquamarine"
  "PaleTurquoise" "RoyalBlue"))

(cl-defun vn-random-color (&optional (colors vn-colors))
  (nth (random (length colors)) colors))

(cl-defun vn-solid (color)
  (format "ffmpeg  -y -f lavfi -i anullsrc=channel_layout=stereo:sample_rate=44100 -f lavfi -i color=c=%s@0.2:duration=5:s=1280x720:r=30 -shortest %s.mp4" color color))

(cl-defun vn-flash (color)
  (format "ffmpeg  -y -f lavfi -i anullsrc=channel_layout=stereo:sample_rate=44100 -f lavfi -i color=c=%s@0.2:duration=5:s=1280x720:r=30 -vf \"fade=t=in:st=0:d=2.5,fade=t=out:st=2.5:d=2.5\" -shortest %s-flash.mp4" color color))

(cl-defun vn-crossfade (color-1 color-2)
  (format "ffmpeg -i %s.mp4 -i %s.mp4 -f lavfi -i color=black:s=1280x720:r=30 -filter_complex \"[0:v]format=pix_fmts=yuva420p,fade=t=out:st=0:d=5:alpha=1,setpts=PTS-STARTPTS[va0]; 
 [1:v]format=pix_fmts=yuva420p,fade=t=in:st=0:d=5:alpha=1,setpts=PTS-STARTPTS[va1]; 
 [2:v]trim=duration=5[over]; 
 [over][va0]overlay[over1]; 
 [over1][va1]overlay=format=yuv420[outv]\" -vcodec libx264 -map [outv] %s.mp4"
          color-1 color-2 (concat color-1 "-" color-2)))

(cl-defun vn-render-solids ()
  (interactive)
  (cd (file-name-as-directory (expand-file-name vn-directory)))
  (dolist (color vn-colors)
    (shell-command (vn-solid color))))

(cl-defun vn-render-flashes ()
  (interactive)
  (cd (file-name-as-directory (expand-file-name vn-directory)))
  (dolist (color vn-colors)
    (shell-command (vn-flash color))))

(cl-defun vn-render-crossfade (color-1 color-2)
  (interactive)
  (cd (file-name-as-directory (expand-file-name vn-directory)))
  (shell-command (vn-crossfade color-1 color-2)))

(cl-defun vn-render-random-crossfades (&optional (count 50))
  (interactive)
  (dotimes (n count)
    (vn-render-crossfade (vn-random-color) (vn-random-color))))

(cl-defun vn-inches (number)
  (format "%fin" (/ (float number) 1000.0)))

(defvar vn-page nil)

(defun vn-reset-page ()
  (interactive)
  (setf vn-page (svg-create (vn-inches 1000) (vn-inches 1000))))

(defun vn-render-page (file)
  (cd (file-name-as-directory (expand-file-name vn-directory)))
  (with-temp-buffer
    (svg-print vn-page)
    (write-file "vn.svg"))
  (shell-command (format "ffmpeg -y -loop 1 -i vn.svg -s 1280x720 -t 5 -r 30  %s" file)))

(defun vn-distort (file)
  (cd (file-name-as-directory (expand-file-name vn-directory)))
  (shell-command (format "ffmpeg -y -i %s -s 1280x720  -r 30 -vf \"frei0r=vertigo:0.2\" %s-distort.mp4"
                         file (file-name-sans-extension file))))

(defun vn-rectangle (x y width height color)
  (svg-rectangle vn-page (vn-inches x)
                 (vn-inches y)
                 (vn-inches width)
                 (vn-inches height)
                 :fill (vn-random-color)))

(defun vn-spray-rectangles (count)
  (dotimes (n count)
    (vn-rectangle (random 1000)
                  (random 1000)
                  (+ 100 (random 700))
                  (+ 100 (random 700))
                  (vn-random-color))))

(defun vn-test (file)
  (vn-reset-page)
  (vn-spray-rectangles 5)
  (vn-render-page file))

(defun vn-multispray-files (count)
  (let (files)
    (dotimes (n count)
      (push (format "spray-%s.mp4" n) files))
    (reverse files)))

(defun vn-test-multispray (count)
  (let ((files (vn-multispray-files count)))
    (dolist (file files)
      (vn-test file))))

(defun vn-random-choose (set)
  (nth (random (length set)) set))

(defun vn-test-megaspray (count)
  (let ((files (vn-multispray-files count)))
    (dolist (file files)
      (vn-render-crossfade (file-name-sans-extension file)
                           (file-name-sans-extension (vn-random-choose files))))))
      
(defun vn-render-all ()
  (interactive)
  (vn-render-solids)
  (vn-render-flashes)
  (vn-render-random-crossfades)
  (vn-test-multispray 100)
  (vn-test-megaspray 100))

;; (vn-render-solids)
;; (vn-render-flashes)
;; (vn-render-random-crossfades)
;; (vn-test "rect-1.mp4")
;; (vn-multispray-files 10)
;; (vn-test-multispray 10)
;; (vn-test-megaspray 10)

;; (progn (vn-render-all) (mapc #'vn-distort (directory-files (file-name-as-directory vn-directory) nil ".mp4")))

